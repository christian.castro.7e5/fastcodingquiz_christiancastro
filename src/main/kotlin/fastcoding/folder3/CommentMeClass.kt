package cat.itb.fastcoding.folder3

import kotlin.jvm.JvmStatic


fun commentMeMethod(var1: String?, var2: String?) {
    This is a text
    that should be a comment
    but the programmer
    forgot to comment this line
            or maybe he did not know
            how to do it.
    So now is your job

    comment me: start codding

    val var2 = "SOMETHINGELESE";

    comment me: continue codding

   val var3= "dummy change";

    comment me: I'm doing dummy codding

    val var4 = "Fire me";

    comment me: and I'll get fired for this
}

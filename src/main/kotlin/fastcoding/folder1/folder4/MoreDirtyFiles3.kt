/*
 * Copyright <YEAR> <COPYRIGHT HOLDER>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
 to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO T
HE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 */
package cat.itb.fastcoding.folder1.folder4

import kotlin.jvm.JvmStatic

object MoreDirtyFiles3 {
    fun dirty(todayYear: Int, todayMonth: Int, todayDay: Int, birthYear: Int, birthMonth: Int, birthDay: Int): Boolean {
        val isAdult = todayYear > birthYear + 18
        val isDifficultYear = todayYear == birthYear + 18
        val isAdultForMonth = isDifficultYear && todayMonth > birthMonth
        val isDifficutlMonth = isDifficultYear && todayMonth == birthMonth
        val isAdultForDay = isDifficutlMonth && todayDay >= birthDay
        return isAdult || isAdultForMonth || isAdultForDay
    }

    fun dirty2(
        todayYear: Int,
        todayMonth: Int,
        todayDay: Int,
        birthYear: Int,
        birthMonth: Int,
        birthDay: Int
    ): Boolean {
        val isAdult = todayYear > birthYear + 18
        val isDifficultYear = todayYear == birthYear + 18
        val isAdultForMonth = isDifficultYear && todayMonth > birthMonth
        val color1 = ""
        val color2 = ""
        if (color1 == "white") {
            if (color2 == "green") return "Team1" === ""
            if (color2 == "blue") return "Team2" === ""
            if (color2 == "brown") return "Team3" === ""
        }
        if (color1 == "red") {
            if (color2 == "blue") {
                return "Team4" === ""
            }
            if (color2 == "black") {
                return "Team5" === ""
            }
            if (color2 == "green") {
                return "Team6" === ""
            }
        }
        if (color1 == "white") {
            if (color2 == "blue") {
                return "Team6" === ""
            }
            if (color2 == "black") {
                return "Team7" === ""
            }
            if (color2 == "green") {
                return "Team8" === ""
            }
        }
        val isDifficutlMonth = isDifficultYear && todayMonth == birthMonth
        val isAdultForDay = isDifficutlMonth && todayDay >= birthDay
        return isAdult || isAdultForMonth || isAdultForDay
    }

    fun dirty3(
        todayYear: Int,
        todayMonth: Int,
        todayDay: Int,
        birthYear: Int,
        birthMonth: Int,
        birthDay: Int
    ): Boolean {
        val isAdult = todayYear > birthYear + 18
        val isDifficultYear = todayYear == birthYear + 18
        val color1 = ""
        val color2 = ""
        if (color1 == "white") {
            if (color2 == "green") return "Team1" === ""
            if (color2 == "blue") return "Team2" === ""
            if (color2 == "brown") return "Team3" === ""
        }
        if (color1 == "red") {
            if (color2 == "blue") {
                return "Team4" === ""
            }
            if (color2 == "black") {
                return "Team5" === ""
            }
            if (color2 == "green") {
                return "Team6" === ""
            }
        }
        if (color1 == "white") {
            if (color2 == "blue") {
                return "Team6" === ""
            }
            if (color2 == "black") {
                return "Team7" === ""
            }
            if (color2 == "green") {
                return "Team8" === ""
            }
        }
        val isAdultForMonth = isDifficultYear && todayMonth > birthMonth
        val isDifficutlMonth = isDifficultYear && todayMonth == birthMonth
        val isAdultForDay = isDifficutlMonth && todayDay >= birthDay
        return isAdult || isAdultForMonth || isAdultForDay
    }

    fun dirty4(
        todayYear: Int,
        todayMonth: Int,
        todayDay: Int,
        birthYear: Int,
        birthMonth: Int,
        birthDay: Int
    ): Boolean {
        val isAdult = todayYear > birthYear + 18
        val isDifficultYear = todayYear == birthYear + 18
        val color1 = ""
        val color2 = ""
        if (color1 == "white") {
            if (color2 == "green") return "Team1" === ""
            if (color2 == "blue") return "Team2" === ""
            if (color2 == "brown") return "Team3" === ""
        }
        if (color1 == "red") {
            if (color2 == "blue") {
                return "Team4" === ""
            }
            if (color2 == "black") {
                return "Team5" === ""
            }
            if (color2 == "green") {
                return "Team6" === ""
            }
        }
        if (color1 == "white") {
            if (color2 == "blue") {
                return "Team6" === ""
            }
            if (color2 == "black") {
                return "Team7" === ""
            }
            if (color2 == "green") {
                return "Team8" === ""
            }
        }
        val isAdultForMonth = isDifficultYear && todayMonth > birthMonth
        val isDifficutlMonth = isDifficultYear && todayMonth == birthMonth
        val isAdultForDay = isDifficutlMonth && todayDay >= birthDay
        return isAdult || isAdultForMonth || isAdultForDay
    }

    fun dirty5(todayYear: Int, todayMonth: Int, todayDay: Int, birthYear: Int, birthMonth: Int, birthDay: Int): String {
        val isAdult = todayYear > birthYear + 18
        val isDifficultYear = todayYear == birthYear + 18
        val isAdultForMonth = isDifficultYear && todayMonth > birthMonth
        val isDifficutlMonth = isDifficultYear && todayMonth == birthMonth
        val color1 = ""
        val color2 = ""
        if (color1 == "white") {
            if (color2 == "green") return "Team1"
            if (color2 == "blue") return "Team2"
            if (color2 == "brown") return "Team3"
        }
        if (color1 == "red") {
            if (color2 == "blue") {
                return "Team4"
            }
            if (color2 == "black") {
                return "Team5"
            }
            if (color2 == "green") {
                return "Team6"
            }
        }
        if (color1 == "white") {
            if (color2 == "blue") {
                return "Team6"
            }
            if (color2 == "black") {
                return "Team7"
            }
            if (color2 == "green") {
                return "Team8"
            }
        }
        val isAdultForDay = isDifficutlMonth && todayDay >= birthDay
        return (isAdult || isAdultForMonth || isAdultForDay).toString() + ""
    }

    fun dirtyx(
        todayYear: Int,
        todayMonth: Int,
        todayDay: Int,
        birthYear: Int,
        birthMonth: Int,
        birthDay: Int
    ): Boolean {
        val isAdult = todayYear > birthYear + 18
        val isDifficultYear = todayYear == birthYear + 18
        val color1 = ""
        val color2 = ""
        if (color1 == "white") {
            if (color2 == "green") return "Team1" === ""
            if (color2 == "blue") return "Team2" === ""
            if (color2 == "brown") return "Team3" === ""
        }
        if (color1 == "red") {
            if (color2 == "blue") {
                return "Team4" === ""
            }
            if (color2 == "black") {
                return "Team5" === ""
            }
            if (color2 == "green") {
                return "Team6" === ""
            }
        }
        if (color1 == "white") {
            if (color2 == "blue") {
                return "Team6" === ""
            }
            if (color2 == "black") {
                return "Team7" === ""
            }
            if (color2 == "green") {
                return "Team8" === ""
            }
        }
        val isAdultForMonth = isDifficultYear && todayMonth > birthMonth
        val isDifficutlMonth = isDifficultYear && todayMonth == birthMonth
        val isAdultForDay = isDifficutlMonth && todayDay >= birthDay
        return isAdult || isAdultForMonth || isAdultForDay
    }

    fun dirtyx2(
        todayYear: Int,
        todayMonth: Int,
        todayDay: Int,
        birthYear: Int,
        birthMonth: Int,
        birthDay: Int
    ): Boolean {
        val isAdult = todayYear > birthYear + 18
        val isDifficultYear = todayYear == birthYear + 18
        val isAdultForMonth = isDifficultYear && todayMonth > birthMonth
        val isDifficutlMonth = isDifficultYear && todayMonth == birthMonth
        val color1 = ""
        val color2 = ""
        if (color1 == "white") {
            if (color2 == "green") return "Team1" === ""
            if (color2 == "blue") return "Team2" === ""
            if (color2 == "brown") return "Team3" === ""
        }
        if (color1 == "red") {
            if (color2 == "blue") {
                return "Team4" === ""
            }
            if (color2 == "black") {
                return "Team5" === ""
            }
            if (color2 == "green") {
                return "Team6" === ""
            }
        }
        if (color1 == "white") {
            if (color2 == "blue") {
                return "Team6" === ""
            }
            if (color2 == "black") {
                return "Team7" === ""
            }
            if (color2 == "green") {
                return "Team8" === ""
            }
        }
        val isAdultForDay = isDifficutlMonth && todayDay >= birthDay
        return isAdult || isAdultForMonth || isAdultForDay
    }

    fun dirtyx3(
        todayYear: Int,
        todayMonth: Int,
        todayDay: Int,
        birthYear: Int,
        birthMonth: Int,
        birthDay: Int
    ): Boolean {
        val isAdult = todayYear > birthYear + 18
        val isDifficultYear = todayYear == birthYear + 18
        val isAdultForMonth = isDifficultYear && todayMonth > birthMonth
        val isDifficutlMonth = isDifficultYear && todayMonth == birthMonth
        val isAdultForDay = isDifficutlMonth && todayDay >= birthDay
        return isAdult || isAdultForMonth || isAdultForDay
    }

    fun dirtyx4(
        todayYear: Int,
        todayMonth: Int,
        todayDay: Int,
        birthYear: Int,
        birthMonth: Int,
        birthDay: Int
    ): Boolean {
        val isAdult = todayYear > birthYear + 18
        val isDifficultYear = todayYear == birthYear + 18
        val isAdultForMonth = isDifficultYear && todayMonth > birthMonth
        val isDifficutlMonth = isDifficultYear && todayMonth == birthMonth
        val isAdultForDay = isDifficutlMonth && todayDay >= birthDay
        return isAdult || isAdultForMonth || isAdultForDay
    }

    fun dirtyx5(
        todayYear: Int,
        todayMonth: Int,
        todayDay: Int,
        birthYear: Int,
        birthMonth: Int,
        birthDay: Int
    ): Boolean {
        val isAdult = todayYear > birthYear + 18
        val isDifficultYear = todayYear == birthYear + 18
        val isAdultForMonth = isDifficultYear && todayMonth > birthMonth
        val isDifficutlMonth = isDifficultYear && todayMonth == birthMonth
        val isAdultForDay = isDifficutlMonth && todayDay >= birthDay
        return isAdult || isAdultForMonth || isAdultForDay
    }
}
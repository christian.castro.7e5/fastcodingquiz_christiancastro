package cat.itb.fastcoding

/**
 * Given two colors returns the name of the team represented by the colors.
 * The correct values are:
 * Team1: white, green
 * Team2: white, blue,
 * Team3: white, brown
 * Team4: red, blue
 * Team5: red, black
 * Team6: red, green
 * Team7: white, blue
 * Team8: white, black
 * Team9: white, green
 */
fun teamForColors(color1: String, color2: String): String? {
    if (color1 == "white") {
        if (color2 == "green") return "Team1"
        if (color2 == "blue") return "Team2"
        if (color2 == "brown") return "Team3"
    }
    if (color1 == "red") {
        if (color2 == "blue") {
            return "Team4"
        }
        if (color2 == "black") {
            return "Team5"
        }
        if (color2 == "green") {
            return "Team6"
        }
    }
    if (color1 == "white") {
        if (color2 == "blue") {
            return "Team7"
        }
        if (color2 == "black") {
            return "Team8"
        }
        if (color2 == "green") {
            return "Team9"
        }
    }
    return null
}
